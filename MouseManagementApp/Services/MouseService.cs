﻿using Microsoft.EntityFrameworkCore;
using MouseManagementApp.Entities;
using MouseManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouseManagementApp.Services
{
    public class MouseService
    {
        private readonly MouseDbContext _db;

        public MouseService(MouseDbContext dbContext)
        {
            this._db = dbContext;
        }

        /// <summary>
        /// This method is used to insert a new mouse product.
        /// </summary>
        /// <param name="mouseModel"></param>
        /// <returns></returns>
        public async Task<bool> InsertNewMouseAsync(MouseModel mouseModel)
        {
            this._db.Add(new Mouse
            {
                MouseName = mouseModel.MouseName,
                MouseQuantity = mouseModel.MouseQuantity
            });
            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// This method is used to update the mouse data based on the MouseId.
        /// </summary>
        /// <param name="mouse"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMouseAsync(MouseModel mouse)
        {
            var mouseModel = await this._db
                .Mouses
                .Where(Q => Q.MouseId == mouse.MouseId)
                .FirstOrDefaultAsync();

            if(mouseModel == null)
            {
                return false;
            }

            mouseModel.MouseName = mouse.MouseName;
            mouseModel.MouseQuantity = mouse.MouseQuantity;

            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// This method is used to delete the mouse data based on the MouseId.
        /// </summary>
        /// <param name="mouseId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMouseAsync(int mouseId)
        {
            var mouseModel = await this._db
                .Mouses
                .Where(Q => Q.MouseId == mouseId)
                .FirstOrDefaultAsync();

            if(mouseModel == null)
            {
                return false;
            }

            this._db.Remove(mouseModel);
            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// This method is used to get all mouse data from the query.
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouseModel>> GetAllMousesAsync()
        {
            var mouses = await this._db
                .Mouses
                .Select(Q => new MouseModel
                {
                    MouseName = Q.MouseName,
                    MouseQuantity = Q.MouseQuantity
                })
                .AsNoTracking()
                .ToListAsync();

            return mouses;
        }

        /// <summary>
        /// This method is used to count the total data of mouses which are stored inside the database, mainly used for pagination process.
        /// </summary>
        /// <returns></returns>
        public int GetTotalData()
        {
            var totalMouse = this._db
                .Mouses
                .Count();

            return totalMouse;
        }

        /// <summary>
        /// This method is used to filter the mouse data by their name.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>
        public async Task<List<MouseModel>> GetFilterDataAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            var query = this._db
                .Mouses
                .AsQueryable();

            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.MouseName.StartsWith(filterByName));
            }

            var mouses = await query
                .Select(Q => new MouseModel
                {
                    MouseId = Q.MouseId,
                    MouseName = Q.MouseName,
                    MouseQuantity = Q.MouseQuantity
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return mouses;
        }

        /// <summary>
        /// This method is used to get a specific mouse data. Used for updating process.
        /// </summary>
        /// <returns></returns>
        public async Task<MouseModel> GetSpecificMouseAsync(int mouseId)
        {
            var mouse = await this._db
                .Mouses
                .Where(Q => Q.MouseId == mouseId)
                .Select(Q => new MouseModel
                {
                    MouseId = Q.MouseId,
                    MouseName = Q.MouseName,
                    MouseQuantity = Q.MouseQuantity
                })
                .FirstOrDefaultAsync();

            return mouse;
        }
    }
}
