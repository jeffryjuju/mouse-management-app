﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MouseManagementApp.Models;
using MouseManagementApp.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MouseManagementApp.APIs
{
    [Route("api/v1/mouse")]
    [ApiController]
    public class MouseApiController : Controller
    {
        private readonly MouseService _mouseMan;
        public MouseApiController(MouseService mouseService)
        {
            this._mouseMan = mouseService;
        }

        /// <summary>
        /// This API is used to get all mouse data.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all-mouse", Name = "getAllMouse")]
        public async Task<ActionResult<List<MouseModel>>> GetAllMouseAsync()
        {
            var mouses = await this._mouseMan.GetAllMousesAsync();
            return Ok(mouses);
        }

        /// <summary>
        /// This API controls the insert method.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost("insert", Name = "insertMouse")]
        public async Task<ActionResult<ResponseModel>> InsertNewMouseAsync([FromBody]MouseModel value)
        {
            var isSuccess = await _mouseMan.InsertNewMouseAsync(value);

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Successfully insert data {value.MouseName}"
            });
        }

        /// <summary>
        /// This API controls the update method.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut("update", Name = "updateMouse")]
        public async Task<ActionResult<ResponseModel>> UpdateMouseAsync([FromBody]MouseModel value)
        {
            var isSuccess = await this._mouseMan.UpdateMouseAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Successfully update data {value.MouseName}"
            });
        }

        /// <summary>
        /// This API controls the delete method.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("delete", Name = "deleteMouse")]
        public async Task<ActionResult<ResponseModel>> DeleteMouseAsync(int mouseId)
        {
            var isSuccess = await this._mouseMan.DeleteMouseAsync(mouseId);

            if(isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Successfully delete mouse data"
            });
        }

        /// <summary>
        /// This API controls the GetTotalData() method.
        /// </summary>
        /// <returns></returns>
        [HttpGet("total-data", Name = "getTotalData")]
        public ActionResult<int> GetTotalData()
        {
            var data = this._mouseMan.GetTotalData();

            return Ok(data);
        }

        /// <summary>
        /// This API controls the filtering method.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>
        [HttpGet("filter-data", Name = "getFilterData")]
        public async Task<ActionResult<List<MouseModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await this._mouseMan.GetFilterDataAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        /// <summary>
        /// This API controls the GetSpecificMouseAsync() method.
        /// </summary>
        /// <param name="mouseId"></param>
        /// <returns></returns>
        [HttpGet("specific-employee", Name = "getSpecificMouse")]
        public async Task<ActionResult<MouseModel>> GetSpecificMouseAsync(int? mouseId)
        {
            if (mouseId.HasValue == false)
            {
                return BadRequest(null);
            }

            var mouse = await _mouseMan.GetSpecificMouseAsync(mouseId.Value);
            if (mouse == null)
            {
                return BadRequest(null);
            }
            return Ok(mouse);
        }
    }
}
