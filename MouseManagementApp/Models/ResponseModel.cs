﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouseManagementApp.Models
{
    public class ResponseModel
    {
        public string ResponseMessage { get; set; }
        public string Status { get; set; }
    }
}
