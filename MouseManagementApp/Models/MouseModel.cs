﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MouseManagementApp.Models
{
    public class MouseModel
    {
        public int MouseId { get; set; }
        
        public string MouseName { get; set; }
        
        public int MouseQuantity { get; set; }
    }
}
