﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MouseManagementApp.Entities
{
    public class Mouse
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Mouse Id")]
        public int MouseId { get; set; }
        [Required]
        [StringLength(7, MinimumLength = 3)]
        [Display(Name = "Mouse Name")]
        public string MouseName { get; set; }
        [Required]
        [Display(Name = "Mouse Quantity")]
        public int MouseQuantity { get; set; }
    }
}
