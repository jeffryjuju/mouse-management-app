﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MouseManagementApp.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "mouse",
                columns: table => new
                {
                    MouseId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MouseName = table.Column<string>(maxLength: 7, nullable: false),
                    MouseQuantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mouse", x => x.MouseId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mouse");
        }
    }
}
