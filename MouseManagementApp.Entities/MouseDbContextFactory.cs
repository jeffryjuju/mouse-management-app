﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace MouseManagementApp.Entities
{
    public class MouseDbContextFactory : IDesignTimeDbContextFactory<MouseDbContext>
    {
        public MouseDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MouseDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new MouseDbContext(builder.Options);
        }
    }
}
