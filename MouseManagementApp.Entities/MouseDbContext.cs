﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MouseManagementApp.Entities
{
    public class MouseDbContext:DbContext
    {
        public MouseDbContext(DbContextOptions<MouseDbContext> options):base(options)
        {

        }

        public DbSet<Mouse> Mouses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Mouse>().ToTable("mouse");
        }
    }
}
